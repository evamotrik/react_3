export const getHomeQuery = () => {
    return fetch("https://jsonplaceholder.typicode.com/posts/1", {
        method: 'PUT',
        body: JSON.stringify({
            id: 1,
            title: "Home Page",
            img: "https://www.powells.com/portals/0/Images/MARQUEE_900_expanded-hours.jpg",
            descr: `Think about the last good book you read. Did it make you feel more connected to others? Maybe it served as a welcome escape. Maybe it helped you rediscover the beauty in life. Did it surprise you?
    As an independent bookstore, we strive to offer the same variety and richness of experience as the books on our shelves. And because the only people we’re beholden to are our customers and ourselves, we can focus on what really matters — promoting diverse perspectives, upholding the free exchange of ideas, championing the enduring power of books, and bolstering the great community of readers and authors we’re lucky to be a part of.
    Thank you for supporting these lofty goals. Your choice sustains a family business, and allows us to follow our passion for getting the right books into the right hands, 365 days a year.`
        }),
        headers: {
            'Content-type': 'application/json; charset=UTF-8'
        }
    })
}