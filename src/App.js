import {Provider} from 'react-redux';
import {store} from './store/store'
import React from 'react';
import Navbar from "./components/NavBar";
import AdminPart from "./components/AdminPart";
import Home from "./components/Home";
import Profile from "./components/Profile";
import UserPart from "./components/UserPart";
import {BrowserRouter} from "react-router-dom";
import {Route} from "react-router-dom";
import './styles/Navbar.module.css';


function App () {
      return (
        <Provider store={store}>
          <BrowserRouter >
          <Navbar />
              <Route path='/home' component={Home} />
              <Route path='/adminPart' component={AdminPart} />
              <Route path='/userPart' component={UserPart} />
              <Route path='/profile' component={Profile} />
          </BrowserRouter>
        </Provider>

         
      )
  }

export default App;
