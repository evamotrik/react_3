import {createStore, combineReducers, applyMiddleware} from "redux";
import bookReducer from "../reducers/book.reducer";
import homeReducer from '../reducers/home.reducer';
import profileReducer from '../reducers/profile.reducer';
import thunk from "redux-thunk";

export const rootReducer = combineReducers({
    home: homeReducer,
    books: bookReducer,
    profile: profileReducer
});

export const store = createStore(rootReducer, applyMiddleware(thunk));