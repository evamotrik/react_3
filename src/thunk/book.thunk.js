import {getHomeQuery} from "../services/home.service";
import {fetchBooksSuccess} from "../actionCreators/books.action";

export const getHomePage = () => (dispatch) => {
    getHomeQuery()
        .then((response) => response.json())
        .then((data) => {
            dispatch(fetchBooksSuccess(data));
        })
        .catch((err) => {
            console.log(err)
        })
};
