const defaultState = 
    {id:1, name:'Ivan', surname:'Ivanovich', card:18345}
;

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'EDIT_USER':
            let newState = {
                name: action.data.name,
                surname: action.data.surname,
                card: action.data.card
            };
            return newState;
        default:
            return state;
    }
};
