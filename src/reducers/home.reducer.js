const defaultState = {
}
export default (state = defaultState, action) => {
    switch (action.type) {
        case 'FETCH_BOOKS_SUCCESS':
            return action.data
        default:
            return state
    }
};
