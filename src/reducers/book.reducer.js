const defaultState = [
    {
        id: 1,
        name: 'Jane Eyre',
        pic: 'https://www.memoriapress.com/wp-content/uploads/2018/02/71ZyqJJ9jL.jpg',
        descr: `Charlotte Brontë, Sylvère Monod. Orphaned as a child, Jane has felt an outcast her whole young life. 
Her courage is tested once again when she arrives at Thornfield Hall, 
where she has been hired by the brooding, proud Edward Rochester to care for his ward Adèle. 
Jane finds herself drawn to his troubled yet kind spirit. She falls in love. Hard.`,
        cost: '12$'
    },

    {
        id: 2,
        name: 'The Picture of Dorian Gray',
        pic: 'https://s1.livelib.ru/boocover/1000218184/o/066d/__The_Picture_of_Dorian_Gray.jpeg',
        descr: `Written in his distinctively dazzling manner, Oscar Wilde’s story of a fashionable young man 
who sells his soul for eternal youth and beauty is the author’s most popular work. 
The tale of Dorian Gray’s moral disintegration caused a scandal when it ﬁrst appeared in 1890, 
but though Wilde was attacked for the novel’s corrupting inﬂuence, he responded that there is, in fact, 
“a terrible moral in Dorian Gray.”`,
        cost: '10$'
    },

    {
        id: 3,
        name: 'Fahrenheit 451',
        pic: 'https://s1.livelib.ru/boocover/1002003580/o/53ef/Ray_Bradbury__Fahrenheit_451.jpeg',
        descr: `Sixty years after its original publication, Ray Bradbury’s internationally acclaimed novel Fahrenheit 451 
stands as a classic of world literature set in a bleak, dystopian future. Today its message has grown more relevant than ever before.`,
        cost: '8$'
    },

    {
        id: 4,
        name: 'Flowers for Algernon',
        pic: 'https://www.pearson.com.au/MC_images/_amazon/9780435232931.jpg',
        descr: `The story of a mentally disabled man whose experimental quest for intelligence mirrors that of Algernon, 
an extraordinary lab mouse. In diary entries, Charlie tells how a brain operation increases his IQ and changes his life. 
As the experimental procedure takes effect, Charlie's intelligence expands until it surpasses that of the doctors who engineered his metamorphosis.`,
        cost: '15$'
    },

    {
        id: 5,
        name: 'Of Human Bondage',
        pic: 'https://kbimages1-a.akamaihd.net/2e69a8c6-a2a7-4317-9efb-c9a20dc511c6/1200/1200/False/of-human-bondage-74.jpg',
        descr: `From a tormented orphan with a clubfoot, Philip Carey grows into an impressionable young man with a voracious appetite 
for adventure and knowledge. His cravings take him to Paris at age eighteen to try his hand at art, then back to London to study medicine. 
But even so, nothing can sate his nagging hunger for experience. Then he falls obsessively in love, embarking on a disastrous relationship that will change his life forever.…`,
        cost: '13$'
    }
]

export default (state = defaultState, action) => {
    switch (action.type) {
        case 'ADD_BOOK':
            return [...state, action.data];
        case 'DELETE_BOOK':
            return state.filter((el) => el.id !== action.id)
        case 'EDIT_BOOK':
            let newState = [];
            state.forEach((el) => {
                if (el.id === action.data.id) {
                    newState.push(action.data)
                } else {
                    newState.push(el)
                }
            });
           return newState;
        default:
            return state;
    }
}
