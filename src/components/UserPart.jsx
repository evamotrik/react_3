import React, { useState } from 'react';
import {connect} from "react-redux";
import style from '../styles/Books.module.css'

function UserPart(props) {
    const [inputValue, setInputValue] = useState('');
    const [selectedName, setSelectedName] = useState('');

    const handleSearch = () => {
        // props.searchBook(inputValue)
        setSelectedName(inputValue);
    }
    
    return (
        <div className={style.Books}>
         <input value={inputValue} onChange={(event) => setInputValue(event.target.value) } />
         <button onClick = {handleSearch}>Search</button>

         {
             props.books.filter((book)=>{
                if(selectedName){
                    return book.name.toLowerCase() === selectedName.toLowerCase() 
                } return true
             })  .map(book => (
                <div className="book" >
                    <h1>{book.name}</h1>
                    <img src={book.pic} alt=""  height="500px" />
                    <div className="descr"  style={{ paddingTop: '30px' }}>{book.descr}</div>
                    <h3>{book.cost}</h3>
                </div>
            ))
        }

        </div>

    )
}

const mapStateToProps = (state) => ({
    books: state.books,
});

export default connect(
    mapStateToProps,
)(UserPart);

