import React, { useEffect, useState } from 'react';
import { connect } from "react-redux";
import {addBook, deleteBook, editBook} from "../actionCreators/books.action";

function AdminPart(props) {
    const [name, setName] = useState();
    const [descr, setDescr] = useState();
    const [pic, setPic] = useState();
    const [cost, setCost] = useState();
    const [id, setId] = useState();
    const [isEdit, setIsEdit] = useState(false);

    const sendNewBook = () => {
        let book = {
            id: isEdit ? id : props.books.length + 1,
            name: name,
            descr: descr,
            pic: pic,
            cost: cost
        };

        if (isEdit) {
            props.editBook(book);
        } else {
            props.addBook(book);
        }

        setName('');
        setDescr('');
        setCost('');
        setPic('');
        setIsEdit(false);
    }

    const handleEdit = (book) => {
        setIsEdit(true);
        setId(book.id);
        setName(book.name);
        setDescr(book.descr);
        setPic(book.pic)
        setCost(book.cost);
    }

    const handleDelete = (book) => {
        props.deleteBook(book.id)
    }

    console.log(props.books)
    return (
        <>
            <h1>Create new product</h1>
            <div className="inputData">
                Name <input value={name} onChange={(e) => setName(e.target.value)} />
                Description <input value={descr} onChange={(e) => setDescr(e.target.value)} />
                Picture <input value={pic} onChange={(e) => setPic(e.target.value)} />
                Cost <input value={cost} onChange={(e) => setCost(e.target.value)} />
                <button onClick={sendNewBook}>Send</button>
            </div>
            {
                props.books.map(book => (
                    <div key={book.id} style={{ textAlign: 'center' }} className="Book">
                        <h2>{book.name}</h2>
                        <div> {book.descr}</div>
                        <img src={book.pic} alt='' height="500px" />
                        <h3>{book.cost} </h3>
                        <button onClick={() => handleEdit(book)}>Change</button>
                        <button onClick={() => handleDelete(book)}>Delete</button>
                    </div>
                ))
            }
        </>
    )
}

const mapStateToProps = (state) => ({
    books: state.books
});

const mapDispatchToProps = (dispatch) => ({
    addBook: (data) => { dispatch(addBook(data)) },
    editBook: (data) => { dispatch(editBook(data)) },
    deleteBook: (id) => { dispatch(deleteBook(id)) },
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AdminPart);
