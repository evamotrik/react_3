import React from 'react';
import {NavLink} from "react-router-dom";
import style from '../styles/Navbar.module.css'
import '..//styles/style.css'

function Navbar() {
        return (
            <nav className={style.Navbar}>
                 <div>
                    <NavLink to='/home'  className={style.text}>Home</NavLink>
                </div>
                <div>
                    <NavLink to='/adminPart'  className={style.text}>Admin Part</NavLink>
                </div>
                <div>
                    <NavLink to='/userPart'  className={style.text}>User Part</NavLink>
                </div>
                <div>
                    <NavLink to='/profile'  className={style.text}>Profile</NavLink>
                </div>
            </nav>
        )
    }


export default Navbar;
