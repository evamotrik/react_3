import React, { useEffect } from 'react';
import { connect } from "react-redux";
import {getHomePage} from "../thunk/book.thunk";


function Home(props) {
    console.log(props);
    useEffect(() => {
        props.onGetHomePage()
    })
    return (
        <div className="home" style={{ textAlign: 'center' }}>
            <h1>{props.home.title}</h1>
            <img src={props.home.img} alt="" style={{ width: '70%', paddingBottom: '35px' }} />
            <div className="homeDescr">{props.home.descr}</div>
        </div>
    )
}

const mapStateToProps = (state) => ({
    home: state.home,
});

const mapDispatchToProps = (dispatch) => ({
    onGetHomePage: () => {dispatch(getHomePage())}
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);
