import React, {useState } from 'react';
import {connect} from "react-redux";


function Profile (props) {

    const [name, setName] = useState(props.profile.name);
    const [surname, setSurname] = useState(props.profile.surname);
    const [card, setCard] = useState(props.profile.card);


    const handleEdit = () => {

        let user = {
            name: name,
            surname: surname,
            card: card
        }

        props.editUser(user);
        setName('');
        setSurname('');
        setCard('')
    }


        console.log(props.users)


        return (
            <div className="profile">
                <h1>Change info</h1>
                <div>
                    <a>Name<input value={name} onChange={(e) => setName(e.target.value)}/></a>
                    <a>Surname<input value={surname} onChange={(e) => setSurname(e.target.value)}/></a>
                    <a>Card<input value={card} onChange={(e) => setCard(e.target.value)}/></a>
                </div>
                <button onClick = {handleEdit}>Change</button>

            </div>
        )
    }


    const mapStateToProps = (state) => ({
        profile: state.profile
    });
    
    const mapDispatchToProps = (dispatch) => ({
        editUser: (data) => {dispatch({type: 'EDIT_USER', data})}
    });
    
    export default connect(
        mapStateToProps,
        mapDispatchToProps
    )(Profile);
