export const addBook = (data) => ({type: 'ADD_BOOK', data});
export const editBook = (data) => ({type: 'EDIT_BOOK', data});
export const deleteBook = (id) => ({type: 'DELETE_BOOK', id});
export const fetchBooksSuccess = (data) => ({type: 'FETCH_BOOKS_SUCCESS', data});
